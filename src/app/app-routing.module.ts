import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ChooserComponent } from "./components/chooser/chooser.component";
import { TableComponent } from "./components/table/table.component";

const routes: Routes = [
  { path: '', component: ChooserComponent },
  { path: 'table/:number', component: TableComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
