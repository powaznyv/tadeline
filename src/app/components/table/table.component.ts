import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {

  number: number = 0
  rdm = Math.floor(Math.random() * 11);

  propositions : string[] = [];
  resultat: string[] = [];

  answer = "";
  isGood = false

  constructor(private _route: ActivatedRoute) { }

  ngOnInit() {
    this.number = this._route.snapshot.params["number"] ;
    this.propositions = [(this.number*this.rdm).toString(), (this.number*(this.rdm-1)).toString(), (this.number*(this.rdm+1)).toString(),  ((this.number-1)*this.rdm).toString()];
    this.propositions.sort(() => Math.random() - 0.5);
  }

  drop(event: CdkDragDrop<string[]>) {
    console.log(event.previousContainer.data);
    console.log(event.container.data);
    console.log(event.previousIndex);
    console.log( event.currentIndex);
    console.log(this.propositions[event.previousIndex]);
    if (this.resultat.length > 0) {
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex);
      transferArrayItem(event.container.data, event.previousContainer.data,
          event.currentIndex+1, event.previousIndex);
    }
    else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
    
    this.answer =  (Number(this.number)*Number(this.rdm)).toString();
  }

  next() {
    this.resultat = [];
    this.rdm = Math.floor(Math.random() * 11);
    this.propositions = [(this.number*this.rdm).toString(), (this.number*(this.rdm-1)).toString(), (this.number*(this.rdm+1)).toString(),  ((this.number-1)*this.rdm).toString()];
    this.propositions.sort(() => Math.random() - 0.5);
  }
}
