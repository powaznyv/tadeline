import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chooser',
  templateUrl: './chooser.component.html',
  styleUrls: ['./chooser.component.scss']
})
export class ChooserComponent {

  constructor(private _router: Router) { }

  firstFunction(table: Number) {

      this._router.navigateByUrl('/table/' + table);
    }
}
